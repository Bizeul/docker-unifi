# This Dockerfile uses Multi-Stage build and requires Docker 17.06

# Main build
# Installs unifi in a regular Debian OS

FROM ubuntu:trusty AS base

LABEL maintainer="Yann Bizeul <yann@tynsoe.org>" 
LABEL original_work="Tuxtof <tuxtof@geo6.net>"

ARG UNIFI_VERSION

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -q update && apt-get install -qy curl && curl -LO https://www.ubnt.com/downloads/unifi/${UNIFI_VERSION}/unifi_sysvinit_all.deb && dpkg -i unifi_sysvinit_all.deb ; apt-get install -fqy && apt-get -q clean && rm -rf /var/lib/apt/lists/*

# Final Container
# Starts with a minimal alpine container and copies files from the debian stage

FROM openjdk:7-alpine

RUN apk update; apk add mongodb

COPY --from=base /usr/lib/unifi /usr/lib/unifi

ADD log4j.properties /usr/lib/unifi/data/

ADD start.sh /

EXPOSE 8080/tcp 8081/tcp 8443/tcp 8843/tcp 8880/tcp 3478/udp

VOLUME ["/usr/lib/unifi/data"]

WORKDIR /usr/lib/unifi

CMD /start.sh
